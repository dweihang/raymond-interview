package raymond.interview;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Question1Test {

    @Test
    public void testCalculate() {
        int actual = Question1.calculate(10, 2, 3);
        int expected = 2 + 5 + 8;
        assertEquals(expected, actual);

        int i = 2;
        List<Integer> arr = new ArrayList<>();
        while (i < 1000) {
            arr.add(i);
            i += 3;
        }
        Iterator<Integer> it = arr.iterator();
        int sum = 0;
        while (it.hasNext()) {
            int val = it.next();
            sum = sum + val;
        }
        assertEquals(sum, Question1.calculate(1000, 2, 3));
    }
}
