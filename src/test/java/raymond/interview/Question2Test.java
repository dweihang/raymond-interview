package raymond.interview;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class Question2Test {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setup() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void teardown() {
        System.setOut(System.out);
    }

    @Test
    public void testWalk() {
        Question2.Animal underTest = new Question2.Cat();
        underTest.walk();
        assertEquals("cat walk", outContent.toString());
        outContent.reset();
        underTest = new Question2.Dog();
        underTest.walk();
        assertEquals("dog walk", outContent.toString());
    }
}
