package raymond.interview;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Question3Test {

    @Test
    public void testWalk() {
        int[] unsorted = {1, 4, 3, 2};
        Question3.Sort(unsorted);
        for (int i= 0; i < unsorted.length;i++) {
            assertEquals(i+1, unsorted[i]);
        }

        int index = Question3.binarySearch(new int[]{0, 1, 2, 3, 33}, 33);
        assertEquals(4, index);

        index = Question3.binarySearch(new int[]{33, 34, 34, 36, 38}, 33);
        assertEquals(0, index);

        index = Question3.binarySearch(new int[]{32, 33, 34, 36, 38}, 33);
        assertEquals(1, index);

        index = Question3.binarySearch(new int[]{31, 32, 33, 34}, 33);
        assertEquals(2, index);
    }
}
