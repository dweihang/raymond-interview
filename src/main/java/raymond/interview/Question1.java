package raymond.interview;

public class Question1 {

    public static int calculate(int maxCapNumber, int startNumber, int step) {
        if (maxCapNumber < startNumber) {
            throw new IllegalArgumentException(String.format("invalid startNumber and maxCampNumber, startNumber=%d, maxNumber=%d", startNumber, maxCapNumber));
        }
        if (step <= 0) {
            throw new IllegalArgumentException(String.format("invalid step, step=%d", step));
        }
        int n = (maxCapNumber - startNumber) / step;
        int endNum = startNumber + step * n;
        return (startNumber + endNum) * (n + 1) / 2;
    }
}