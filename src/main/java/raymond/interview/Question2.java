package raymond.interview;

public class Question2 {

    public static abstract class Animal {
        public abstract void walk();
    }

    public static class Cat extends Animal{

        @Override
        public void walk() {
            System.out.print("cat walk");
        }
    }

    public static class Dog extends Animal{

        @Override
        public void walk() {
            System.out.print("dog walk");
        }
    }
}
