package raymond.interview;

public class Question4 {

    public static String SQL1() {
        return "select * from A where name like '%Chan'";
    }

    public static String SQL2() {
        return "select * from A where name like '%Chan' and id in (select id from B where age > 20)";
    }
}
