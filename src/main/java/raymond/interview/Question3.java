package raymond.interview;

import java.util.Arrays;

public class Question3 {

    public static void Sort(int[] arr) {
        Arrays.sort(arr);
    }

    public static int binarySearch(int []arr, int targetNum)  {
        if (arr.length == 0) {
           throw new IllegalArgumentException("empty array");
        }
        if (arr.length == 1) {
            return arr[0];
        }
        if (arr[arr.length - 1] == targetNum) {
            return arr.length - 1;
        }
        if (arr[0] == targetNum) {
            return 0;
        }
        int lowerBound = 0;
        int upperBound = arr.length - 1;
        int index = (lowerBound + upperBound) / 2;
        while (true) {
            if (arr[index] == targetNum) {
                return index;
            } else if (arr[index] > targetNum) {
                if (index - lowerBound == 1) {
                    break;
                }
                upperBound = index;
                index = (lowerBound + index) / 2;
            } else {
                if (upperBound - index == 1) {
                    break;
                }
                lowerBound = index;
                index = (upperBound + index) / 2;
            }
        }
        return -1;
    }
}
